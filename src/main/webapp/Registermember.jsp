<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Title</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
  <form action="Register" method="post" class="d-flex justify-content-center m-3">
    <div class="card col-5  ">
      <div class="card-body ">
        <table class="form-group">
          <tr class="form-group">
            <th >Enter Your Name : </th> 
            <td><input class="form-control" type="text" name="name" required></td>
          </tr>
          <tr class="form-group mt-2">
            <th>Enter Your Passwod : </th> 
            <td><input class="form-control" type="text" name="password" required></td>
          </tr>
          <tr class="form-group">
            <th>Enter Your Email : </th> 
            <td><input class="form-control" type="text" name="email" required></td>
          </tr>
          <tr class="form-group">
            <th>Enter Your PhoneNumber : </th> 
            <td><input class="form-control " type="number" name="number"  required></td>
          </tr>
        </table>
        <div class="d-flex justify-content-around mt-3">
          <input  type="submit" value="Register">
        </div>
      </div>
    </div>
   </form>
  
    <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>