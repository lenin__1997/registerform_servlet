import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegisterDao {
	
	private String dbUrl = "jdbc:mysql://localhost:3306/userdb";
	private String dbname = "root";
	private String dbpassword = "root";
	private String dbDriver = "com.mysql.cj.jdbc.Driver";
	
	public void loadDriver(String dbDriver) {
			try {
				Class.forName(dbDriver);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		
	}
	public Connection getConnection(){
		Connection conection =null;
		try {
			conection = DriverManager.getConnection(dbUrl,dbname,dbpassword);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return conection;
	}
	
	public String insert(Member member) {
		loadDriver(dbDriver);
		Connection connect = getConnection();
		String result = "Data Entered Successfully";
		String sql = "insert into member values(?,?,?,?)";
		PreparedStatement preState;
		try {
			preState = connect.prepareStatement(sql);
			preState.setNString(1, member.getName());
			preState.setNString(2, member.getPassword());
			preState.setNString(3, member.getEmail());
			preState.setNString(4, member.getNumber());
			preState.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result= "Data not Entered";
		}
		return result;
	}

}
